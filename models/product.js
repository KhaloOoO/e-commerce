var mongoose=require('mongoose');
var productSchema=mongoose.Schema(
    {
        name:{type:String,required:true},
        description:{type:String, required:true},
        image:{type:String ,required:true},
        price:{type:Number,required:true},
        size:{type:Number,required:true}
    }
);
module.exports=mongoose.model("product",productSchema);