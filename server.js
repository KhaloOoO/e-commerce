//required modules
var express = require('express')
var app = express();
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var session = require('express-session');
var sessionMongo = require('connect-mongo')(session);
var csrf = require('csurf');
var User = require('./models/User');
var csrfProtection = csrf();
var passport = require('passport');
require('./config/passport');
var flash = require('connect-flash');
var validator = require('express-validator');
var product = require('./models/product');
var Cart = require('./models/Cart');
//
app.use(express.static(__dirname + '/public'));
app.set('view engine', 'ejs');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(validator());


mongoose.connect('mongodb://localhost/buy-it');

mongoose.Promise = global.Promise;

app.use(session(
    {
        secret: 'mysupersecret',
        resave: false,
        saveUninitialized: false,
        store: new sessionMongo({mongooseConnection: mongoose.connection}),
        cookie: {maxAge: 1000 * 400 * 400}
    }
));
//app.use(csrfProtection);

app.use(flash());
app.use(passport.initialize());
app.use(passport.session());


app.use(function (req, res, next) {
    res.locals.auth = req.isAuthenticated();
    res.locals.session = req.session;
    res.locals.userInfo = req.user;
    next();
});

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/login');
}
var users = require('./routes/userRoutes')(app);
//home page route

app.get('/home', function (req, res) {
    product.find({}, function (err, result) {
        if (err)
            console.log(err)
        res.render(__dirname + '/views/home', {allProducts: result, guest: false});
    });

});
app.get('/cart/remove/:id', function (req, res) {
    var id = req.params.id;

    function getValues(obj, toFind) {
        var i = 0, key;
        for (key in obj) {
            if (key == toFind) {
                return i;
            }
            i++;
        }
        return null;
    }

    var index = getValues(req.session.cart.items, id);

});

//add to cart route
app.get('/add/:id', isLoggedIn, function (req, res) {
    var id = req.params.id;
    var cart = new Cart(req.session.cart ? req.session.cart : {});
    product.findById(id, function (err, product) {
        if (err)
            console.log(err)
        cart.addFunction(product, product.id);
        req.session.cart = cart;
        res.redirect('/home');
    });
});
//cart route
app.get('/shoping-cart', isLoggedIn, function (req, res) {
    if (!req.session.cart) {
        return res.render(__dirname + '/views/finalCart', {allProductsInCart: null});
    }
    var cart = new Cart(req.session.cart);
    res.render(__dirname + '/views/finalCart', {
        allProductsInCart: cart.getAll(),
        totalPrice: cart.totalPrice,
        guest: false
    });
});
app.listen('3000');