var User = require('../models/User').User;
const fileUpload = require('express-fileupload');

exports.updateProfile = function (req, res) {
    var userId = req.user.id;
    if (!req.files)
        return res.status(400).send('No files were uploaded.');

    User.findByIdAndUpdate(userId, req.body, function (err, result) {
        if (err) return res.json({message: "Error in updating person with id " + req.params.id});
        res.redirect('/user/profile');
    });
}

