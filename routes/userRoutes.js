var path = require('path');
var passport = require('passport');
var userController = require('../controllers/UserController');


module.exports = function (app) {
    app.get('/signup', function (req, res) {
        var messages = req.flash('error');
        var guest = true;
        res.render(path.join(__dirname, '../views/signup.ejs'), {
            messages: messages,
            hasErrors: messages.length > 0,
            guest: guest
        });
    });
    app.post('/signup', passport.authenticate('local.signup',
        {
            successRedirect: '/home',
            failureRedirect: '/signup',
            failureFlash: true
        }
    ));

    app.get('/login', function (req, res) {
        var messages = req.flash('error');
        var guest = true;
        res.render(path.join(__dirname, '../views/login.ejs'), {
            messages: messages,
            hasErrors: messages.length > 0,
            guest: guest
        });
    });
    app.post('/login', passport.authenticate('local.signin', {
        successRedirect: '/home',
        failureRedirect: '/login',
        failureFlash: true
    }));

    app.get('/logout', function (req, res, next) {
        req.logout();
        res.redirect('/login');
    });

    app.get('/user/profile', function (req, res, next) {
        res.render(path.join(__dirname, '../views/profile.ejs'));
    });

    app.get('/user/profile/edit', function (req, res, next) {
        var messages = req.flash('error');

        res.render(path.join(__dirname, '../views/editProfile.ejs'), {
            messages: messages,
            hasErrors: messages.length > 0,
        });
    });

    app.post('/user/profile/update',userController.updateProfile);

}